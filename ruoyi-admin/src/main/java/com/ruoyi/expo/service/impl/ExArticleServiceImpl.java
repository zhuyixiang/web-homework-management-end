package com.ruoyi.expo.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.expo.mapper.ExArticleMapper;
import com.ruoyi.expo.domain.ExArticle;
import com.ruoyi.expo.service.IExArticleService;

/**
 * 文章Service业务层处理
 * 
 * @author 我可莉害了
 * @date 2023-09-20
 */
@Service
public class ExArticleServiceImpl implements IExArticleService 
{
    @Autowired
    private ExArticleMapper exArticleMapper;

    /**
     * 查询文章
     * 
     * @param id 文章主键
     * @return 文章
     */
    @Override
    public ExArticle selectExArticleById(Long id)
    {
        return exArticleMapper.selectExArticleById(id);
    }

    /**
     * 查询文章列表
     * 
     * @param exArticle 文章
     * @return 文章
     */
    @Override
    public List<ExArticle> selectExArticleList(ExArticle exArticle)
    {
        return exArticleMapper.selectExArticleList(exArticle);
    }

    /**
     * 新增文章
     * 
     * @param exArticle 文章
     * @return 结果
     */
    @Override
    public int insertExArticle(ExArticle exArticle)
    {
        exArticle.setCreateTime(DateUtils.getNowDate());
        return exArticleMapper.insertExArticle(exArticle);
    }

    /**
     * 修改文章
     * 
     * @param exArticle 文章
     * @return 结果
     */
    @Override
    public int updateExArticle(ExArticle exArticle)
    {
        exArticle.setUpdateTime(DateUtils.getNowDate());
        return exArticleMapper.updateExArticle(exArticle);
    }

    /**
     * 批量删除文章
     * 
     * @param ids 需要删除的文章主键
     * @return 结果
     */
    @Override
    public int deleteExArticleByIds(Long[] ids)
    {
        return exArticleMapper.deleteExArticleByIds(ids);
    }

    /**
     * 删除文章信息
     * 
     * @param id 文章主键
     * @return 结果
     */
    @Override
    public int deleteExArticleById(Long id)
    {
        return exArticleMapper.deleteExArticleById(id);
    }
}
