package com.ruoyi.expo.mapper;

import java.util.List;
import com.ruoyi.expo.domain.ExArticle;

/**
 * 文章Mapper接口
 * 
 * @author 我可莉害了
 * @date 2023-09-20
 */
public interface ExArticleMapper 
{
    /**
     * 查询文章
     * 
     * @param id 文章主键
     * @return 文章
     */
    public ExArticle selectExArticleById(Long id);

    /**
     * 查询文章列表
     * 
     * @param exArticle 文章
     * @return 文章集合
     */
    public List<ExArticle> selectExArticleList(ExArticle exArticle);

    /**
     * 新增文章
     * 
     * @param exArticle 文章
     * @return 结果
     */
    public int insertExArticle(ExArticle exArticle);

    /**
     * 修改文章
     * 
     * @param exArticle 文章
     * @return 结果
     */
    public int updateExArticle(ExArticle exArticle);

    /**
     * 删除文章
     * 
     * @param id 文章主键
     * @return 结果
     */
    public int deleteExArticleById(Long id);

    /**
     * 批量删除文章
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteExArticleByIds(Long[] ids);
}
