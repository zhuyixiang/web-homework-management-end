package com.ruoyi.restaurant.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.SecurityUtils;
import com.ruoyi.restaurant.domain.ResShop;
import com.ruoyi.restaurant.service.IResShopService;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.restaurant.domain.ResDishes;
import com.ruoyi.restaurant.service.IResDishesService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 菜品Controller
 * 
 * @author 文豪
 * @date 2023-10-11
 */
@RestController
@RequestMapping("/restaurant/dishes")
public class ResDishesController extends BaseController
{
    @Autowired
    private IResDishesService resDishesService;

    @Autowired
    private IResShopService resShopService;

    /**
     * 查询菜品列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:dishes:list')")
    @GetMapping("/list")
    public TableDataInfo list(ResDishes resDishes)
    {
        //1、找到当前用户的userID
        Long userId= SecurityUtils.getUserId();
        //2、找到当前用户管理的店铺
        if (!SecurityUtils.isAdmin(userId)) {
            ResShop resShop = new ResShop();
            resShop.setOwnerId(userId);
            List<ResShop> shopList = resShopService.selectResShopList(resShop);
            if (shopList.size() == 0) {
                throw new RuntimeException("该用户没有管理的店铺");
            } else {
                ResShop shop = shopList.get(0);
                resDishes.setShopId(shop.getId());
            }
        }





        startPage();
        List<ResDishes> list = resDishesService.selectResDishesList(resDishes);
        return getDataTable(list);
    }

    /**
     * 导出菜品列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:dishes:export')")
    @Log(title = "菜品", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResDishes resDishes)
    {
        List<ResDishes> list = resDishesService.selectResDishesList(resDishes);
        ExcelUtil<ResDishes> util = new ExcelUtil<ResDishes>(ResDishes.class);
        util.exportExcel(response, list, "菜品数据");
    }

    /**
     * 获取菜品详细信息
     */
    @PreAuthorize("@ss.hasPermi('restaurant:dishes:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(resDishesService.selectResDishesById(id));
    }

    /**
     * 新增菜品
     */
    @PreAuthorize("@ss.hasPermi('restaurant:dishes:add')")
    @Log(title = "菜品", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResDishes resDishes)
    {
        return toAjax(resDishesService.insertResDishes(resDishes));
    }

    /**
     * 修改菜品
     */
    @PreAuthorize("@ss.hasPermi('restaurant:dishes:edit')")
    @Log(title = "菜品", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResDishes resDishes)
    {
        return toAjax(resDishesService.updateResDishes(resDishes));
    }

    /**
     * 删除菜品
     */
    @PreAuthorize("@ss.hasPermi('restaurant:dishes:remove')")
    @Log(title = "菜品", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(resDishesService.deleteResDishesByIds(ids));
    }
}
