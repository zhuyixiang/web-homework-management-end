package com.ruoyi.restaurant.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.restaurant.domain.ResCar;
import com.ruoyi.restaurant.service.IResCarService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 购物车Controller
 * 
 * @author 朱翼翔
 * @date 2023-12-12
 */
@RestController
@RequestMapping("/restaurant/car")
public class ResCarController extends BaseController
{
    @Autowired
    private IResCarService resCarService;

    /**
     * 查询购物车列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:car:list')")
    @GetMapping("/list")
    public TableDataInfo list(ResCar resCar)
    {
        startPage();
        List<ResCar> list = resCarService.selectResCarList(resCar);
        return getDataTable(list);
    }

    /**
     * 导出购物车列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:car:export')")
    @Log(title = "购物车", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResCar resCar)
    {
        List<ResCar> list = resCarService.selectResCarList(resCar);
        ExcelUtil<ResCar> util = new ExcelUtil<ResCar>(ResCar.class);
        util.exportExcel(response, list, "购物车数据");
    }

    /**
     * 获取购物车详细信息
     */
    @PreAuthorize("@ss.hasPermi('restaurant:car:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(resCarService.selectResCarById(id));
    }

    /**
     * 新增购物车
     */
    @PreAuthorize("@ss.hasPermi('restaurant:car:add')")
    @Log(title = "购物车", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResCar resCar)
    {
        return toAjax(resCarService.insertResCar(resCar));
    }

    /**
     * 修改购物车
     */
    @PreAuthorize("@ss.hasPermi('restaurant:car:edit')")
    @Log(title = "购物车", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResCar resCar)
    {
        return toAjax(resCarService.updateResCar(resCar));
    }

    /**
     * 删除购物车
     */
    @PreAuthorize("@ss.hasPermi('restaurant:car:remove')")
    @Log(title = "购物车", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(resCarService.deleteResCarByIds(ids));
    }
}
