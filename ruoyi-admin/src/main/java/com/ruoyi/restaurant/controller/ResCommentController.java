package com.ruoyi.restaurant.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.restaurant.domain.ResComment;
import com.ruoyi.restaurant.service.IResCommentService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 评论Controller
 * 
 * @author 朱翼翔
 * @date 2023-12-19
 */
@RestController
@RequestMapping("/restaurant/comment")
public class ResCommentController extends BaseController
{
    @Autowired
    private IResCommentService resCommentService;

    /**
     * 查询评论列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:comment:list')")
    @GetMapping("/list")
    public TableDataInfo list(ResComment resComment)
    {
        startPage();
        List<ResComment> list = resCommentService.selectResCommentList(resComment);
        return getDataTable(list);
    }

    /**
     * 导出评论列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:comment:export')")
    @Log(title = "评论", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResComment resComment)
    {
        List<ResComment> list = resCommentService.selectResCommentList(resComment);
        ExcelUtil<ResComment> util = new ExcelUtil<ResComment>(ResComment.class);
        util.exportExcel(response, list, "评论数据");
    }

    /**
     * 获取评论详细信息
     */
    @PreAuthorize("@ss.hasPermi('restaurant:comment:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(resCommentService.selectResCommentById(id));
    }

    /**
     * 新增评论
     */
    @PreAuthorize("@ss.hasPermi('restaurant:comment:add')")
    @Log(title = "评论", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResComment resComment)
    {
        return toAjax(resCommentService.insertResComment(resComment));
    }

    /**
     * 修改评论
     */
    @PreAuthorize("@ss.hasPermi('restaurant:comment:edit')")
    @Log(title = "评论", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResComment resComment)
    {
        return toAjax(resCommentService.updateResComment(resComment));
    }

    /**
     * 删除评论
     */
    @PreAuthorize("@ss.hasPermi('restaurant:comment:remove')")
    @Log(title = "评论", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(resCommentService.deleteResCommentByIds(ids));
    }
}
