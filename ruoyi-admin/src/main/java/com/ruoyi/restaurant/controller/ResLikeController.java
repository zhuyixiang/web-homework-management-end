package com.ruoyi.restaurant.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.restaurant.domain.ResLike;
import com.ruoyi.restaurant.service.IResLikeService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 点赞Controller
 * 
 * @author 朱翼翔
 * @date 2023-12-19
 */
@RestController
@RequestMapping("/restaurant/like")
public class ResLikeController extends BaseController
{
    @Autowired
    private IResLikeService resLikeService;

    /**
     * 查询点赞列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:like:list')")
    @GetMapping("/list")
    public TableDataInfo list(ResLike resLike)
    {
        startPage();
        List<ResLike> list = resLikeService.selectResLikeList(resLike);
        return getDataTable(list);
    }

    /**
     * 导出点赞列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:like:export')")
    @Log(title = "点赞", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResLike resLike)
    {
        List<ResLike> list = resLikeService.selectResLikeList(resLike);
        ExcelUtil<ResLike> util = new ExcelUtil<ResLike>(ResLike.class);
        util.exportExcel(response, list, "点赞数据");
    }

    /**
     * 获取点赞详细信息
     */
    @PreAuthorize("@ss.hasPermi('restaurant:like:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(resLikeService.selectResLikeById(id));
    }

    /**
     * 新增点赞
     */
    @PreAuthorize("@ss.hasPermi('restaurant:like:add')")
    @Log(title = "点赞", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResLike resLike)
    {
        return toAjax(resLikeService.insertResLike(resLike));
    }

    /**
     * 修改点赞
     */
    @PreAuthorize("@ss.hasPermi('restaurant:like:edit')")
    @Log(title = "点赞", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResLike resLike)
    {
        return toAjax(resLikeService.updateResLike(resLike));
    }

    /**
     * 删除点赞
     */
    @PreAuthorize("@ss.hasPermi('restaurant:like:remove')")
    @Log(title = "点赞", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(resLikeService.deleteResLikeByIds(ids));
    }
}
