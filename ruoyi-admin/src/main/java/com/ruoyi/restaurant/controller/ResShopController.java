package com.ruoyi.restaurant.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;

import com.ruoyi.common.utils.SecurityUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.restaurant.domain.ResShop;
import com.ruoyi.restaurant.service.IResShopService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 店铺Controller
 * 
 * @author 文豪
 * @date 2023-09-27
 */
@RestController
@RequestMapping("/restaurant/shop")
public class ResShopController extends BaseController
{
    @Autowired
    private IResShopService resShopService;

    /**
     * 查询店铺列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:shop:list')")
    @GetMapping("/list")
    public TableDataInfo list(ResShop resShop)
    {
        //1要获取当前用户的userid
        Long userId= SecurityUtils.getUserId();
        if(!SecurityUtils.isAdmin(userId)){
            resShop.setOwnerId(userId);
        }
        startPage();
        List<ResShop> list = resShopService.selectResShopList(resShop);
        return getDataTable(list);
    }

    /**
     * 导出店铺列表
     */
    @PreAuthorize("@ss.hasPermi('restaurant:shop:export')")
    @Log(title = "店铺", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ResShop resShop)
    {
        List<ResShop> list = resShopService.selectResShopList(resShop);
        ExcelUtil<ResShop> util = new ExcelUtil<ResShop>(ResShop.class);
        util.exportExcel(response, list, "店铺数据");
    }

    /**
     * 获取店铺详细信息
     */
    @PreAuthorize("@ss.hasPermi('restaurant:shop:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(resShopService.selectResShopById(id));
    }

    /**
     * 新增店铺
     */
    @PreAuthorize("@ss.hasPermi('restaurant:shop:add')")
    @Log(title = "店铺", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ResShop resShop)
    {
        return toAjax(resShopService.insertResShop(resShop));
    }

    /**
     * 修改店铺
     */
    @PreAuthorize("@ss.hasPermi('restaurant:shop:edit')")
    @Log(title = "店铺", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ResShop resShop)
    {
        return toAjax(resShopService.updateResShop(resShop));
    }

    /**
     * 删除店铺
     */
    @PreAuthorize("@ss.hasPermi('restaurant:shop:remove')")
    @Log(title = "店铺", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(resShopService.deleteResShopByIds(ids));
    }
}
