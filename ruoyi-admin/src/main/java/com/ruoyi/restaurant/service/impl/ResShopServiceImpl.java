package com.ruoyi.restaurant.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.restaurant.mapper.ResShopMapper;
import com.ruoyi.restaurant.domain.ResShop;
import com.ruoyi.restaurant.service.IResShopService;

/**
 * 店铺Service业务层处理
 * 
 * @author 文豪
 * @date 2023-09-27
 */
@Service
public class ResShopServiceImpl implements IResShopService 
{
    @Autowired
    private ResShopMapper resShopMapper;

    /**
     * 查询店铺
     * 
     * @param id 店铺主键
     * @return 店铺
     */
    @Override
    public ResShop selectResShopById(Long id)
    {
        return resShopMapper.selectResShopById(id);
    }

    /**
     * 查询店铺列表
     * 
     * @param resShop 店铺
     * @return 店铺
     */
    @Override
    public List<ResShop> selectResShopList(ResShop resShop)
    {
        return resShopMapper.selectResShopList(resShop);
    }

    /**
     * 新增店铺
     * 
     * @param resShop 店铺
     * @return 结果
     */
    @Override
    public int insertResShop(ResShop resShop)
    {
        resShop.setCreateTime(DateUtils.getNowDate());
        return resShopMapper.insertResShop(resShop);
    }

    /**
     * 修改店铺
     * 
     * @param resShop 店铺
     * @return 结果
     */
    @Override
    public int updateResShop(ResShop resShop)
    {
        resShop.setUpdateTime(DateUtils.getNowDate());
        return resShopMapper.updateResShop(resShop);
    }

    /**
     * 批量删除店铺
     * 
     * @param ids 需要删除的店铺主键
     * @return 结果
     */
    @Override
    public int deleteResShopByIds(Long[] ids)
    {
        return resShopMapper.deleteResShopByIds(ids);
    }

    /**
     * 删除店铺信息
     * 
     * @param id 店铺主键
     * @return 结果
     */
    @Override
    public int deleteResShopById(Long id)
    {
        return resShopMapper.deleteResShopById(id);
    }
}
