package com.ruoyi.restaurant.service;

import java.util.List;
import com.ruoyi.restaurant.domain.ResLike;

/**
 * 点赞Service接口
 * 
 * @author 朱翼翔
 * @date 2023-12-19
 */
public interface IResLikeService 
{
    /**
     * 查询点赞
     * 
     * @param id 点赞主键
     * @return 点赞
     */
    public ResLike selectResLikeById(Long id);

    /**
     * 查询点赞列表
     * 
     * @param resLike 点赞
     * @return 点赞集合
     */
    public List<ResLike> selectResLikeList(ResLike resLike);

    /**
     * 新增点赞
     * 
     * @param resLike 点赞
     * @return 结果
     */
    public int insertResLike(ResLike resLike);

    /**
     * 修改点赞
     * 
     * @param resLike 点赞
     * @return 结果
     */
    public int updateResLike(ResLike resLike);

    /**
     * 批量删除点赞
     * 
     * @param ids 需要删除的点赞主键集合
     * @return 结果
     */
    public int deleteResLikeByIds(Long[] ids);

    /**
     * 删除点赞信息
     * 
     * @param id 点赞主键
     * @return 结果
     */
    public int deleteResLikeById(Long id);
}
