package com.ruoyi.restaurant.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.restaurant.mapper.ResCarMapper;
import com.ruoyi.restaurant.domain.ResCar;
import com.ruoyi.restaurant.service.IResCarService;

/**
 * 购物车Service业务层处理
 * 
 * @author 朱翼翔
 * @date 2023-12-12
 */
@Service
public class ResCarServiceImpl implements IResCarService 
{
    @Autowired
    private ResCarMapper resCarMapper;

    /**
     * 查询购物车
     * 
     * @param id 购物车主键
     * @return 购物车
     */
    @Override
    public ResCar selectResCarById(Long id)
    {
        return resCarMapper.selectResCarById(id);
    }

    /**
     * 查询购物车列表
     * 
     * @param resCar 购物车
     * @return 购物车
     */
    @Override
    public List<ResCar> selectResCarList(ResCar resCar)
    {
        return resCarMapper.selectResCarList(resCar);
    }

    /**
     * 新增购物车
     * 
     * @param resCar 购物车
     * @return 结果
     */
    @Override
    public int insertResCar(ResCar resCar)
    {
        resCar.setCreateTime(DateUtils.getNowDate());
        return resCarMapper.insertResCar(resCar);
    }

    /**
     * 修改购物车
     * 
     * @param resCar 购物车
     * @return 结果
     */
    @Override
    public int updateResCar(ResCar resCar)
    {
        resCar.setUpdateTime(DateUtils.getNowDate());
        return resCarMapper.updateResCar(resCar);
    }

    /**
     * 批量删除购物车
     * 
     * @param ids 需要删除的购物车主键
     * @return 结果
     */
    @Override
    public int deleteResCarByIds(Long[] ids)
    {
        return resCarMapper.deleteResCarByIds(ids);
    }

    /**
     * 删除购物车信息
     * 
     * @param id 购物车主键
     * @return 结果
     */
    @Override
    public int deleteResCarById(Long id)
    {
        return resCarMapper.deleteResCarById(id);
    }
}
