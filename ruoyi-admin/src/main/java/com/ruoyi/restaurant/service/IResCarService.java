package com.ruoyi.restaurant.service;

import java.util.List;
import com.ruoyi.restaurant.domain.ResCar;

/**
 * 购物车Service接口
 * 
 * @author 朱翼翔
 * @date 2023-12-12
 */
public interface IResCarService 
{
    /**
     * 查询购物车
     * 
     * @param id 购物车主键
     * @return 购物车
     */
    public ResCar selectResCarById(Long id);

    /**
     * 查询购物车列表
     * 
     * @param resCar 购物车
     * @return 购物车集合
     */
    public List<ResCar> selectResCarList(ResCar resCar);

    /**
     * 新增购物车
     * 
     * @param resCar 购物车
     * @return 结果
     */
    public int insertResCar(ResCar resCar);

    /**
     * 修改购物车
     * 
     * @param resCar 购物车
     * @return 结果
     */
    public int updateResCar(ResCar resCar);

    /**
     * 批量删除购物车
     * 
     * @param ids 需要删除的购物车主键集合
     * @return 结果
     */
    public int deleteResCarByIds(Long[] ids);

    /**
     * 删除购物车信息
     * 
     * @param id 购物车主键
     * @return 结果
     */
    public int deleteResCarById(Long id);
}
