package com.ruoyi.restaurant.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.restaurant.mapper.ResCommentMapper;
import com.ruoyi.restaurant.domain.ResComment;
import com.ruoyi.restaurant.service.IResCommentService;

/**
 * 评论Service业务层处理
 * 
 * @author 朱翼翔
 * @date 2023-12-19
 */
@Service
public class ResCommentServiceImpl implements IResCommentService 
{
    @Autowired
    private ResCommentMapper resCommentMapper;

    /**
     * 查询评论
     * 
     * @param id 评论主键
     * @return 评论
     */
    @Override
    public ResComment selectResCommentById(Long id)
    {
        return resCommentMapper.selectResCommentById(id);
    }

    /**
     * 查询评论列表
     * 
     * @param resComment 评论
     * @return 评论
     */
    @Override
    public List<ResComment> selectResCommentList(ResComment resComment)
    {
        return resCommentMapper.selectResCommentList(resComment);
    }

    /**
     * 新增评论
     * 
     * @param resComment 评论
     * @return 结果
     */
    @Override
    public int insertResComment(ResComment resComment)
    {
        resComment.setCreateTime(DateUtils.getNowDate());
        return resCommentMapper.insertResComment(resComment);
    }

    /**
     * 修改评论
     * 
     * @param resComment 评论
     * @return 结果
     */
    @Override
    public int updateResComment(ResComment resComment)
    {
        resComment.setUpdateTime(DateUtils.getNowDate());
        return resCommentMapper.updateResComment(resComment);
    }

    /**
     * 批量删除评论
     * 
     * @param ids 需要删除的评论主键
     * @return 结果
     */
    @Override
    public int deleteResCommentByIds(Long[] ids)
    {
        return resCommentMapper.deleteResCommentByIds(ids);
    }

    /**
     * 删除评论信息
     * 
     * @param id 评论主键
     * @return 结果
     */
    @Override
    public int deleteResCommentById(Long id)
    {
        return resCommentMapper.deleteResCommentById(id);
    }
}
