package com.ruoyi.restaurant.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.restaurant.mapper.ResDishesMapper;
import com.ruoyi.restaurant.domain.ResDishes;
import com.ruoyi.restaurant.service.IResDishesService;

/**
 * 菜品Service业务层处理
 * 
 * @author 文豪
 * @date 2023-10-11
 */
@Service
public class ResDishesServiceImpl implements IResDishesService 
{
    @Autowired
    private ResDishesMapper resDishesMapper;

    /**
     * 查询菜品
     * 
     * @param id 菜品主键
     * @return 菜品
     */
    @Override
    public ResDishes selectResDishesById(Long id)
    {
        return resDishesMapper.selectResDishesById(id);
    }

    /**
     * 查询菜品列表
     * 
     * @param resDishes 菜品
     * @return 菜品
     */
    @Override
    public List<ResDishes> selectResDishesList(ResDishes resDishes)
    {
        return resDishesMapper.selectResDishesList(resDishes);
    }

    /**
     * 新增菜品
     * 
     * @param resDishes 菜品
     * @return 结果
     */
    @Override
    public int insertResDishes(ResDishes resDishes)
    {
        resDishes.setCreateTime(DateUtils.getNowDate());
        return resDishesMapper.insertResDishes(resDishes);
    }

    /**
     * 修改菜品
     * 
     * @param resDishes 菜品
     * @return 结果
     */
    @Override
    public int updateResDishes(ResDishes resDishes)
    {
        resDishes.setUpdateTime(DateUtils.getNowDate());
        return resDishesMapper.updateResDishes(resDishes);
    }

    /**
     * 批量删除菜品
     * 
     * @param ids 需要删除的菜品主键
     * @return 结果
     */
    @Override
    public int deleteResDishesByIds(Long[] ids)
    {
        return resDishesMapper.deleteResDishesByIds(ids);
    }

    /**
     * 删除菜品信息
     * 
     * @param id 菜品主键
     * @return 结果
     */
    @Override
    public int deleteResDishesById(Long id)
    {
        return resDishesMapper.deleteResDishesById(id);
    }
}
