package com.ruoyi.restaurant.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.restaurant.mapper.ResBannerMapper;
import com.ruoyi.restaurant.domain.ResBanner;
import com.ruoyi.restaurant.service.IResBannerService;

/**
 * 轮播图Service业务层处理
 * 
 * @author 文豪
 * @date 2023-09-27
 */
@Service
public class ResBannerServiceImpl implements IResBannerService 
{
    @Autowired
    private ResBannerMapper resBannerMapper;

    /**
     * 查询轮播图
     * 
     * @param id 轮播图主键
     * @return 轮播图
     */
    @Override
    public ResBanner selectResBannerById(Long id)
    {
        return resBannerMapper.selectResBannerById(id);
    }

    /**
     * 查询轮播图列表
     * 
     * @param resBanner 轮播图
     * @return 轮播图
     */
    @Override
    public List<ResBanner> selectResBannerList(ResBanner resBanner)
    {
        return resBannerMapper.selectResBannerList(resBanner);
    }

    /**
     * 新增轮播图
     * 
     * @param resBanner 轮播图
     * @return 结果
     */
    @Override
    public int insertResBanner(ResBanner resBanner)
    {
        resBanner.setCreateTime(DateUtils.getNowDate());
        return resBannerMapper.insertResBanner(resBanner);
    }

    /**
     * 修改轮播图
     * 
     * @param resBanner 轮播图
     * @return 结果
     */
    @Override
    public int updateResBanner(ResBanner resBanner)
    {
        resBanner.setUpdateTime(DateUtils.getNowDate());
        return resBannerMapper.updateResBanner(resBanner);
    }

    /**
     * 批量删除轮播图
     * 
     * @param ids 需要删除的轮播图主键
     * @return 结果
     */
    @Override
    public int deleteResBannerByIds(Long[] ids)
    {
        return resBannerMapper.deleteResBannerByIds(ids);
    }

    /**
     * 删除轮播图信息
     * 
     * @param id 轮播图主键
     * @return 结果
     */
    @Override
    public int deleteResBannerById(Long id)
    {
        return resBannerMapper.deleteResBannerById(id);
    }
}
