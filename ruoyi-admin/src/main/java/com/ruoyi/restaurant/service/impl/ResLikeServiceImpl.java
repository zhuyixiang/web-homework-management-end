package com.ruoyi.restaurant.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.restaurant.mapper.ResLikeMapper;
import com.ruoyi.restaurant.domain.ResLike;
import com.ruoyi.restaurant.service.IResLikeService;

/**
 * 点赞Service业务层处理
 * 
 * @author 朱翼翔
 * @date 2023-12-19
 */
@Service
public class ResLikeServiceImpl implements IResLikeService 
{
    @Autowired
    private ResLikeMapper resLikeMapper;

    /**
     * 查询点赞
     * 
     * @param id 点赞主键
     * @return 点赞
     */
    @Override
    public ResLike selectResLikeById(Long id)
    {
        return resLikeMapper.selectResLikeById(id);
    }

    /**
     * 查询点赞列表
     * 
     * @param resLike 点赞
     * @return 点赞
     */
    @Override
    public List<ResLike> selectResLikeList(ResLike resLike)
    {
        return resLikeMapper.selectResLikeList(resLike);
    }

    /**
     * 新增点赞
     * 
     * @param resLike 点赞
     * @return 结果
     */
    @Override
    public int insertResLike(ResLike resLike)
    {
        return resLikeMapper.insertResLike(resLike);
    }

    /**
     * 修改点赞
     * 
     * @param resLike 点赞
     * @return 结果
     */
    @Override
    public int updateResLike(ResLike resLike)
    {
        return resLikeMapper.updateResLike(resLike);
    }

    /**
     * 批量删除点赞
     * 
     * @param ids 需要删除的点赞主键
     * @return 结果
     */
    @Override
    public int deleteResLikeByIds(Long[] ids)
    {
        return resLikeMapper.deleteResLikeByIds(ids);
    }

    /**
     * 删除点赞信息
     * 
     * @param id 点赞主键
     * @return 结果
     */
    @Override
    public int deleteResLikeById(Long id)
    {
        return resLikeMapper.deleteResLikeById(id);
    }
}
