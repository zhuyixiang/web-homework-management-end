package com.ruoyi.restaurant.mapper;

import java.util.List;
import com.ruoyi.restaurant.domain.ResShop;

/**
 * 店铺Mapper接口
 * 
 * @author 文豪
 * @date 2023-09-27
 */
public interface ResShopMapper 
{
    /**
     * 查询店铺
     * 
     * @param id 店铺主键
     * @return 店铺
     */
    public ResShop selectResShopById(Long id);

    /**
     * 查询店铺列表
     * 
     * @param resShop 店铺
     * @return 店铺集合
     */
    public List<ResShop> selectResShopList(ResShop resShop);

    /**
     * 新增店铺
     * 
     * @param resShop 店铺
     * @return 结果
     */
    public int insertResShop(ResShop resShop);

    /**
     * 修改店铺
     * 
     * @param resShop 店铺
     * @return 结果
     */
    public int updateResShop(ResShop resShop);

    /**
     * 删除店铺
     * 
     * @param id 店铺主键
     * @return 结果
     */
    public int deleteResShopById(Long id);

    /**
     * 批量删除店铺
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResShopByIds(Long[] ids);
}
