package com.ruoyi.restaurant.mapper;

import java.util.List;
import com.ruoyi.restaurant.domain.ResDishes;

/**
 * 菜品Mapper接口
 * 
 * @author 文豪
 * @date 2023-10-11
 */
public interface ResDishesMapper 
{
    /**
     * 查询菜品
     * 
     * @param id 菜品主键
     * @return 菜品
     */
    public ResDishes selectResDishesById(Long id);

    /**
     * 查询菜品列表
     * 
     * @param resDishes 菜品
     * @return 菜品集合
     */
    public List<ResDishes> selectResDishesList(ResDishes resDishes);

    /**
     * 新增菜品
     * 
     * @param resDishes 菜品
     * @return 结果
     */
    public int insertResDishes(ResDishes resDishes);

    /**
     * 修改菜品
     * 
     * @param resDishes 菜品
     * @return 结果
     */
    public int updateResDishes(ResDishes resDishes);

    /**
     * 删除菜品
     * 
     * @param id 菜品主键
     * @return 结果
     */
    public int deleteResDishesById(Long id);

    /**
     * 批量删除菜品
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResDishesByIds(Long[] ids);
}
