package com.ruoyi.restaurant.mapper;

import java.util.List;
import com.ruoyi.restaurant.domain.ResBanner;

/**
 * 轮播图Mapper接口
 * 
 * @author 文豪
 * @date 2023-09-27
 */
public interface ResBannerMapper 
{
    /**
     * 查询轮播图
     * 
     * @param id 轮播图主键
     * @return 轮播图
     */
    public ResBanner selectResBannerById(Long id);

    /**
     * 查询轮播图列表
     * 
     * @param resBanner 轮播图
     * @return 轮播图集合
     */
    public List<ResBanner> selectResBannerList(ResBanner resBanner);

    /**
     * 新增轮播图
     * 
     * @param resBanner 轮播图
     * @return 结果
     */
    public int insertResBanner(ResBanner resBanner);

    /**
     * 修改轮播图
     * 
     * @param resBanner 轮播图
     * @return 结果
     */
    public int updateResBanner(ResBanner resBanner);

    /**
     * 删除轮播图
     * 
     * @param id 轮播图主键
     * @return 结果
     */
    public int deleteResBannerById(Long id);

    /**
     * 批量删除轮播图
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResBannerByIds(Long[] ids);
}
