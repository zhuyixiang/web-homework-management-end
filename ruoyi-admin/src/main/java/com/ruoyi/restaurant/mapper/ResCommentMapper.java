package com.ruoyi.restaurant.mapper;

import java.util.List;
import com.ruoyi.restaurant.domain.ResComment;

/**
 * 评论Mapper接口
 * 
 * @author 朱翼翔
 * @date 2023-12-19
 */
public interface ResCommentMapper 
{
    /**
     * 查询评论
     * 
     * @param id 评论主键
     * @return 评论
     */
    public ResComment selectResCommentById(Long id);

    /**
     * 查询评论列表
     * 
     * @param resComment 评论
     * @return 评论集合
     */
    public List<ResComment> selectResCommentList(ResComment resComment);

    /**
     * 新增评论
     * 
     * @param resComment 评论
     * @return 结果
     */
    public int insertResComment(ResComment resComment);

    /**
     * 修改评论
     * 
     * @param resComment 评论
     * @return 结果
     */
    public int updateResComment(ResComment resComment);

    /**
     * 删除评论
     * 
     * @param id 评论主键
     * @return 结果
     */
    public int deleteResCommentById(Long id);

    /**
     * 批量删除评论
     * 
     * @param ids 需要删除的数据主键集合
     * @return 结果
     */
    public int deleteResCommentByIds(Long[] ids);
}
