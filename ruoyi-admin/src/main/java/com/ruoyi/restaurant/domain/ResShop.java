package com.ruoyi.restaurant.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 店铺对象 res_shop
 * 
 * @author 文豪
 * @date 2023-09-27
 */
public class ResShop extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 店铺名称 */
    @Excel(name = "店铺名称")
    private String name;

    /** 店铺地址 */
    @Excel(name = "店铺地址")
    private String address;

    /** 联系电话 */
    @Excel(name = "联系电话")
    private String contact;

    /** 封面 */
    @Excel(name = "封面")
    private String cover;

    /** 店铺详情 */
    private String detail;

    /** 评分 */
    @Excel(name = "评分")
    private Long star;

    /** 店主id */
    @Excel(name = "店主id")
    private Long ownerId;



    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setName(String name) 
    {
        this.name = name;
    }

    public String getName() 
    {
        return name;
    }
    public void setAddress(String address) 
    {
        this.address = address;
    }

    public String getAddress() 
    {
        return address;
    }
    public void setContact(String contact) 
    {
        this.contact = contact;
    }

    public String getContact() 
    {
        return contact;
    }
    public void setCover(String cover) 
    {
        this.cover = cover;
    }

    public String getCover() 
    {
        return cover;
    }
    public void setDetail(String detail) 
    {
        this.detail = detail;
    }

    public String getDetail() 
    {
        return detail;
    }
    public void setStar(Long star) 
    {
        this.star = star;
    }

    public Long getStar() 
    {
        return star;
    }
    public void setOwnerId(Long ownerId) 
    {
        this.ownerId = ownerId;
    }

    public Long getOwnerId() 
    {
        return ownerId;
    }



    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("name", getName())
            .append("address", getAddress())
            .append("contact", getContact())
            .append("cover", getCover())
            .append("detail", getDetail())
            .append("star", getStar())
            .append("ownerId", getOwnerId())
            .append("createBy", getCreateBy())
            .append("createTime", getCreateTime())
            .append("updateBy", getUpdateBy())
            .append("updateTime", getUpdateTime())

            .toString();
    }
}
