package com.ruoyi.restaurant.domain;

import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.common.annotation.Excel;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 轮播图对象 res_banner
 * 
 * @author 文豪
 * @date 2023-09-27
 */
public class ResBanner extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 编号 */
    private Long id;

    /** 标题 */
    @Excel(name = "标题")
    private String title;

    /** 广告图片 */
    @Excel(name = "广告图片")
    private String pic;

    /** 跳转链接 */
    @Excel(name = "跳转链接")
    private String targetUrl;

    /** 排序 */
    @Excel(name = "排序")
    private Long sort;

    /** 状态(1:正常，0:禁用) */
    @Excel(name = "状态(1:正常，0:禁用)")
    private String status;

    public void setId(Long id) 
    {
        this.id = id;
    }

    public Long getId() 
    {
        return id;
    }
    public void setTitle(String title) 
    {
        this.title = title;
    }

    public String getTitle() 
    {
        return title;
    }
    public void setPic(String pic) 
    {
        this.pic = pic;
    }

    public String getPic() 
    {
        return pic;
    }
    public void setTargetUrl(String targetUrl) 
    {
        this.targetUrl = targetUrl;
    }

    public String getTargetUrl() 
    {
        return targetUrl;
    }
    public void setSort(Long sort) 
    {
        this.sort = sort;
    }

    public Long getSort() 
    {
        return sort;
    }
    public void setStatus(String status) 
    {
        this.status = status;
    }

    public String getStatus() 
    {
        return status;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("id", getId())
            .append("title", getTitle())
            .append("pic", getPic())
            .append("targetUrl", getTargetUrl())
            .append("sort", getSort())
            .append("status", getStatus())
            .append("createTime", getCreateTime())
            .append("updateTime", getUpdateTime())
            .toString();
    }
}
