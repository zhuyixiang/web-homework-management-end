package com.ruoyi.system.controller;

import java.util.List;
import javax.servlet.http.HttpServletResponse;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.ExImage;
import com.ruoyi.system.service.IExImageService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 轮播图Controller
 * 
 * @author 文豪
 * @date 2023-09-20
 */
@RestController
@RequestMapping("/image/image")
public class ExImageController extends BaseController
{
    @Autowired
    private IExImageService exImageService;

    /**
     * 查询轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('image:image:list')")
    @GetMapping("/list")
    public TableDataInfo list(ExImage exImage)
    {
        startPage();
        List<ExImage> list = exImageService.selectExImageList(exImage);
        return getDataTable(list);
    }

    /**
     * 导出轮播图列表
     */
    @PreAuthorize("@ss.hasPermi('image:image:export')")
    @Log(title = "轮播图", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    public void export(HttpServletResponse response, ExImage exImage)
    {
        List<ExImage> list = exImageService.selectExImageList(exImage);
        ExcelUtil<ExImage> util = new ExcelUtil<ExImage>(ExImage.class);
        util.exportExcel(response, list, "轮播图数据");
    }

    /**
     * 获取轮播图详细信息
     */
    @PreAuthorize("@ss.hasPermi('image:image:query')")
    @GetMapping(value = "/{id}")
    public AjaxResult getInfo(@PathVariable("id") Long id)
    {
        return success(exImageService.selectExImageById(id));
    }

    /**
     * 新增轮播图
     */
    @PreAuthorize("@ss.hasPermi('image:image:add')")
    @Log(title = "轮播图", businessType = BusinessType.INSERT)
    @PostMapping
    public AjaxResult add(@RequestBody ExImage exImage)
    {
        return toAjax(exImageService.insertExImage(exImage));
    }

    /**
     * 修改轮播图
     */
    @PreAuthorize("@ss.hasPermi('image:image:edit')")
    @Log(title = "轮播图", businessType = BusinessType.UPDATE)
    @PutMapping
    public AjaxResult edit(@RequestBody ExImage exImage)
    {
        return toAjax(exImageService.updateExImage(exImage));
    }

    /**
     * 删除轮播图
     */
    @PreAuthorize("@ss.hasPermi('image:image:remove')")
    @Log(title = "轮播图", businessType = BusinessType.DELETE)
	@DeleteMapping("/{ids}")
    public AjaxResult remove(@PathVariable Long[] ids)
    {
        return toAjax(exImageService.deleteExImageByIds(ids));
    }
}
