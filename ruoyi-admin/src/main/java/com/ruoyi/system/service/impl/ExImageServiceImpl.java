package com.ruoyi.system.service.impl;

import java.util.List;
import com.ruoyi.common.utils.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.system.mapper.ExImageMapper;
import com.ruoyi.system.domain.ExImage;
import com.ruoyi.system.service.IExImageService;

/**
 * 轮播图Service业务层处理
 * 
 * @author 文豪
 * @date 2023-09-20
 */
@Service
public class ExImageServiceImpl implements IExImageService 
{
    @Autowired
    private ExImageMapper exImageMapper;

    /**
     * 查询轮播图
     * 
     * @param id 轮播图主键
     * @return 轮播图
     */
    @Override
    public ExImage selectExImageById(Long id)
    {
        return exImageMapper.selectExImageById(id);
    }

    /**
     * 查询轮播图列表
     * 
     * @param exImage 轮播图
     * @return 轮播图
     */
    @Override
    public List<ExImage> selectExImageList(ExImage exImage)
    {
        return exImageMapper.selectExImageList(exImage);
    }

    /**
     * 新增轮播图
     * 
     * @param exImage 轮播图
     * @return 结果
     */
    @Override
    public int insertExImage(ExImage exImage)
    {
        exImage.setCreateTime(DateUtils.getNowDate());
        return exImageMapper.insertExImage(exImage);
    }

    /**
     * 修改轮播图
     * 
     * @param exImage 轮播图
     * @return 结果
     */
    @Override
    public int updateExImage(ExImage exImage)
    {
        exImage.setUpdateTime(DateUtils.getNowDate());
        return exImageMapper.updateExImage(exImage);
    }

    /**
     * 批量删除轮播图
     * 
     * @param ids 需要删除的轮播图主键
     * @return 结果
     */
    @Override
    public int deleteExImageByIds(Long[] ids)
    {
        return exImageMapper.deleteExImageByIds(ids);
    }

    /**
     * 删除轮播图信息
     * 
     * @param id 轮播图主键
     * @return 结果
     */
    @Override
    public int deleteExImageById(Long id)
    {
        return exImageMapper.deleteExImageById(id);
    }
}
