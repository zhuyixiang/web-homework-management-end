import request from '@/utils/request'

// 查询菜品列表
export function listDishes(query) {
  return request({
    url: '/restaurant/dishes/list',
    method: 'get',
    params: query
  })
}

// 查询菜品详细
export function getDishes(id) {
  return request({
    url: '/restaurant/dishes/' + id,
    method: 'get'
  })
}

// 新增菜品
export function addDishes(data) {
  return request({
    url: '/restaurant/dishes',
    method: 'post',
    data: data
  })
}

// 修改菜品
export function updateDishes(data) {
  return request({
    url: '/restaurant/dishes',
    method: 'put',
    data: data
  })
}

// 删除菜品
export function delDishes(id) {
  return request({
    url: '/restaurant/dishes/' + id,
    method: 'delete'
  })
}
