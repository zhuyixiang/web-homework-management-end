import request from '@/utils/request'

// 查询轮播图列表
export function listImage(query) {
  return request({
    url: '/image/image/list',
    method: 'get',
    params: query
  })
}

// 查询轮播图详细
export function getImage(id) {
  return request({
    url: '/image/image/' + id,
    method: 'get'
  })
}

// 新增轮播图
export function addImage(data) {
  return request({
    url: '/image/image',
    method: 'post',
    data: data
  })
}

// 修改轮播图
export function updateImage(data) {
  return request({
    url: '/image/image',
    method: 'put',
    data: data
  })
}

// 删除轮播图
export function delImage(id) {
  return request({
    url: '/image/image/' + id,
    method: 'delete'
  })
}
